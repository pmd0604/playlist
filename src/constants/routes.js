export const LANDING = '/';
export const HOME = '/';
export const ACCOUNT = '/account';
export const ADMIN = '/admin';
export const PLAYLIST = '/playlist'
export const PASSWORD_FORGET = '/pw-forget';
