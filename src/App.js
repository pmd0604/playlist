import React, { Component } from 'react'
import './App.css'
import Navigation from './component/Navigation'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import * as ROUTES from './constants/routes'
import HomePage from './component/HomePage'
import DetailUser from './component/DetailUser'
import Playlist from './component/playlist'
import youtube from './apis/youtube'
import Container from 'react-bootstrap/Container'
import base, { firebaseApp } from './config/fireBase'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchInput: '',
      videos: [],
      user: {},
      showNotification: false

    }
  }
  componentDidMount() {
    firebaseApp.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user: user })
      } else {
        this.setState({ user: {} })
      }
    })
  }
  handleOnClick = id => {
    let ref = base.push(`users`, {
      data: { userId: this.state.user.uid, videoId: id },
      then(err) {
        if (err) {
          console.log(err)
        }
      }
    })
    this.setState({ showNotification: true })
  }

  closeNotification = () => {
    this.setState({ showNotification: false })
  }

  handleChangeSearchInput = e => {
    this.setState({
      searchInput: e.target.value
    })
  }
  handleSearchSubmit = async e => {
    e.preventDefault()

    const response = await youtube.get('/search', {
      params: {
        q: this.state.searchInput
      }
    })
    this.setState({
      videos: response.data.items
    })
  }

  render() {
    let { searchInput, videos, showNotification } = this.state

    return (
      <Router>
        <div className="App">
          <Navigation
            searchInput={searchInput}
            handleChangeSearchInput={this.handleChangeSearchInput}
            handleSearchSubmit={this.handleSearchSubmit}
            closeNotification={this.closeNotification}
            showNotification={showNotification}

          />
          <Container>
            <Route
              path={ROUTES.HOME}
              component={() => (
                <HomePage videos={videos} handleOnClick={this.handleOnClick} />
              )}
            />
            <Route path={ROUTES.ACCOUNT} component={DetailUser} />
            <Route path={ROUTES.PLAYLIST}
              component={() => (
                <Playlist handleOnClick={this.getPlaylist} />
              )}
            />
          </Container>
        </div>
      </Router>
    )
  }
}
export default App;
