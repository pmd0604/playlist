import React from 'react';
import { render } from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
// import './fontawesome';

render(<App />, document.getElementById('root'));
registerServiceWorker();
