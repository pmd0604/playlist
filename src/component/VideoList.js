import React from 'react'
import VideoItem from './VideoItem'

function VideoList(props) {
    const videos = props.videos
    const listItem = videos.map(video => (
        <VideoItem video={video} key={video.id.videoId} handleOnClick={props.handleOnClick} />
    ))

    return <React.Fragment>{listItem}</React.Fragment>
}

export default VideoList
