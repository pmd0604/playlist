import React, { Component, } from "react";
import firebase from 'firebase';
import { Form, Button } from 'react-bootstrap';





class detailUser extends Component {


    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: null,
            displayName: null,
            photoURL: null,
            lastSignInTime: null,
        }
    }
    componentWillUnmount() {
        this.sate = {
            loading: this.props.loading,
            email: this.props.email,
            displayName: this.props.displayName,
            photoURL: this.props.photoURL,
            lastSignInTime: this.props.lastSignInTime,
        }
    }
    componentDidMount() {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.authHandler({ user })
            }
        })

    }
    authHandler = async authData => {
        const user = authData.user
        this.setState({
            email: user.email,
            displayName: user.displayName,
            photoURL: user.photoURL,
            lastSignInTime: user.metadata.lastSignInTime.toString()
        })
    }


    render() {
        return (
            <div className='body'>
                <h2>Your Profile</h2>
                <img src={this.state.photoURL} alt='avatar' />

                <Form.Group controlId="formInfo" id='formInfo'>
                    <Form.Label>Full name</Form.Label>
                    <Form.Control type="text" placeholder={this.state.displayName} />
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="text" placeholder={this.state.email} disabled />
                    <Button variant="primary" type="submit">Save</Button>
                </Form.Group>


            </div>
        )
    }

}
export default detailUser;
