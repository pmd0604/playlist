import React, { Component, Fragment } from "react";
import firebase from "firebase";
import Login from "./Login";
import base, { firebaseApp } from "../config/fireBase";
import { NavDropdown, } from 'react-bootstrap';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

import * as ROUTES from '../constants/routes';


class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      displayName: null,
      photoURL: null,
      lastSignInTime: null,
    }
  }


  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.authHandler({ user });
      }
    });
  }

  authHandler = async authData => {
    const user = authData.user;

    this.setState({
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      lastSignInTime: user.metadata.lastSignInTime.toString()
    });

  };

  authenticate = provider => {

    const authProvider = new firebase.auth[`${provider}AuthProvider`]();
    firebaseApp
      .auth()
      .signInWithPopup(authProvider)
      .then(this.authHandler);
  };

  logout = async () => {
    await firebase.auth().signOut();
    this.setState({ email: null, displayName: null });
  };


  render() {
    const logout = <p onClick={this.logout}>Log Out</p>;
    if (!this.state.email) {
      return <Login authenticate={this.authenticate} />;
    }
    const menuUser = <div>
      <img src={this.state.photoURL} alt='avatar' />
      <span> {this.state.displayName} </span>
    </div>;
    return (

      <Fragment>
        <NavDropdown className='user-info' title={menuUser} id="basic-nav-dropdown">
          <NavDropdown.Item ><Link to={ROUTES.ACCOUNT}>Profiles</Link></NavDropdown.Item>
          <NavDropdown.Item ><Link to={ROUTES.PLAYLIST}>My playlist</Link></NavDropdown.Item>
          <NavDropdown.Item >{logout}</NavDropdown.Item>
        </NavDropdown>
      </Fragment>


    );
  }
}

export default UserInfo;
