import React from 'react'
import Toast from 'react-bootstrap/Toast'

function Notification({ show, closeNotification }) {
    return (
        <Toast
            show={show}
            style={{
                position: 'fixed',
                zIndex: '99999',
                top: 0,
                right: 0
            }}
            onClose={closeNotification}
        >
            <Toast.Header>
                <img src="holder.js/20x20?text=%20" className="rounded mr-2" alt="" />
                <strong className="mr-auto">New Video Has been Add to Playlist</strong>
            </Toast.Header>
            <Toast.Body>Your video has been added to playlist</Toast.Body>
        </Toast>
    )
}

export default Notification
