import React from 'react'
import VideoList from './VideoList'
import Row from 'react-bootstrap/Row'
// import VideoDetail from './VideoDetail'




class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            selectedVideo: null
        }
    }

    handleOnClick = (video) => {
        this.setState({ selectedVideo: video })
    }

    // function HomePage(props) {
    render() {
        return (
            <Row>
                {this.props.videos.length > 0 ? (
                    <VideoList videos={this.props.videos} handleOnClick={this.props.handleOnClick} />
                ) : (
                        <h2>Nothing</h2>
                    )}
            </Row>

        )
    }
}

export default HomePage
