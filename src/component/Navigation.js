import React, { Component } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import { Form, Button, FormControl, row } from 'react-bootstrap'
import UserInfo from './UserInfo'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import * as ROUTES from '../constants/routes'
import Notification from './Notification'

class Navigation extends Component {
    render() {
        const {
            searchInput,
            handleChangeSearchInput,
            handleSearchSubmit,
            showNotification,
            closeNotification
        } = this.props
        return (
            <>
                {showNotification ? (
                    <Notification closeNotification={closeNotification} />
                ) : (
                        <div />
                    )}
                <Navbar className="Navbar" bg="light" expand="lg">
                    <div className="row">
                        <div className="col-md-2 col-lg-2">
                            <Link to={ROUTES.HOME}>
                                <Navbar.Brand>
                                    <span>
                                        <img
                                            src="https://i.pinimg.com/originals/de/1c/91/de1c91788be0d791135736995109272a.png"
                                            alt="logo"
                                        />
                                    </span>
                                    <span>Playlist</span>
                                </Navbar.Brand>
                            </Link>
                        </div>
                        <div className="col-md-8 col-lg-8">
                            <Form className="search" onSubmit={handleSearchSubmit}>
                                <ul>
                                    <li className="search-input">
                                        <FormControl
                                            type="text"
                                            placeholder="Search"
                                            className="mr-sm-2"
                                            onChange={handleChangeSearchInput}
                                            value={searchInput}
                                        />
                                    </li>
                                    <li className="search-button">
                                        <Button
                                            variant="outline-success"
                                            onClick={handleSearchSubmit}
                                        >
                                            Search
                                        </Button>
                                    </li>
                                </ul>
                            </Form>
                        </div>
                        <div className="col-md-2 col-lg-2">
                            <UserInfo />
                        </div>
                    </div>
                </Navbar>
            </>
        )
    }
}
export default Navigation;
