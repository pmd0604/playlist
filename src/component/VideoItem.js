import React from 'react'
import '../style/video.css'
import Col from 'react-bootstrap/Col'

const VideoItem = ({ video, handleOnClick }) => {
    return (
        <Col className="video-item item" xs={4} onClick={() => handleOnClick(video.id.videoId)}>
            <div>
                <img className="ui image" src={video.snippet.thumbnails.medium.url} alt={video.snippet.description} />
            </div>

            <div className="content">
                <div className="header">{video.snippet.title}</div>
            </div>
        </Col>
    )
}
export default VideoItem
