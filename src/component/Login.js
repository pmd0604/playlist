import React from "react";


const Login = props => (
    <div className="login">
        <button id="google" onClick={() => props.authenticate("Google")} className='btn btn-outline-primary'>
            <img src="https://ezig.github.io/assets/img/google.png" alt="google login" />
            Log In
        </button>
    </div>
);

export default Login;
