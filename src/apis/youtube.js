import axios from 'axios'

let instance = axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3/'
})

instance.interceptors.request.use(
    config => ({
        ...config,
        params: {
            part: 'snippet',
            maxResults: '12',
            key: 'AIzaSyDhmLu4DyzLjScHsVx2keLCfJPXTJ0GxZ8',
            ...config.params
        }
    }),
    function (error) {
        return Promise.reject(error)
    }
)

export default instance
